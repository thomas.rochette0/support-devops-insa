exports.order = function order(req, res, next) {
  // TODO implement from here

  console.log("Requête : ")
  console.log(req.body)

  // tableau des Taxes
  var taxes = {"DE":20, "UK":21, "FR":20, "IT":25, "ES":19, "PL":21, "RO":20, "NL":20, "BE":24, "EL":20,"CZ":19,"PT":23,"HU":27,"SE":23,"AT":22,"BG":21,"DK":21,"FI":17,"SK":18,"IE":21,"HR":23,"LT":23,"SI":24,"LV":20,"EE":22,"CY":21,"LU":25,"MT":20,};

  if (!("prices" in req.body) || !("quantities" in req.body) || !("country" in req.body) || !("reduction" in req.body)){
    return res.status(400).end()
  } else if (req.body.quantities.length != req.body.prices.length) {
    return res.status(400).end()
  } else if (!(req.body.country in taxes)) {
    return res.status(400).end()
  }

  // Prix Hors Taxes
  var totalHT = 0;

  // Boucle de calcul du prix HT
  var i;
  for(i=0; i<req.body.quantities.length;i++)
  {
    totalHT = totalHT + (req.body.prices[i]*req.body.quantities[i])
  }

  // Calcul du Total TTC
  var totalTTC
  if(req.body.country == "BEL"){
    if(totalHT<2000){
      totalTTC = totalHT
    }else{
      totalTTC = (totalHT + totalHT * (taxes[req.body.country]/100))
    }
  } else if(req.body.country == "SKU"){
    if(totalHT>2000){
      totalTTC = totalHT
    }else{
      totalTTC = (totalHT + totalHT * (taxes[req.body.country]/100))
    }
  } else if(req.body.country == "UKU"){
    totalTTC = (totalHT - totalHT * (taxes[req.body.country]/100))
  }else {
    totalTTC = (totalHT + totalHT * (taxes[req.body.country]/100))
  }

  var discount = 1

  if(req.body.reduction == "PAY THE PRICE"){
    discount = 1
  }else if(req.body.reduction == "STANDARD"){
    if (totalTTC>=50000){
      discount = 0.85
    }else if (totalTTC>=10000){
      discount = 0.90
    }else if (totalTTC>=7000){
      discount = 0.93
    }else if (totalTTC>=5000){
      discount = 0.95
    }else if (totalTTC>=1000){
      discount = 0.97
    } else {
      discount = 1
    }
    //discount = 1
  }else if (req.body.reduction == "HALF PRICE"){
    discount=0.5
  }


  var totalDiscount = totalTTC*discount
  res.json({"total":totalDiscount});

  console.log("Totaldis : "+discount)
  console.log("Total : "+totalTTC)
  console.log("Result : ")
  console.log(res.body)
};

exports.feedback = function feedback(req, res, next) {
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
};
